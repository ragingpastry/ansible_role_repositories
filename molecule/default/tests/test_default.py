""" Modules for molecule testing ansible_role_repositories """

import os
import testinfra.utils.ansible_runner
import pytest


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("rhel_repos", [("/etc/yum.repos.d/rhel.repo")])
def test_for_rhel_repo_file(host, rhel_repos):
    """
    Test to ensure that the centos test didn't get the rhel repos installed
    """
    rhel = host.file(rhel_repos)
    assert rhel.exists is False


@pytest.mark.parametrize(
    "repo_file",
    [("/etc/yum.repos.d/epel.repo"), ("/etc/yum.repos.d/ius.repo")],
)
def test_for_repo_files(host, repo_file):
    """
    Test for the existence of the repo files that were either installed
    via he repo package, or by the configuration variables
    """
    repo = host.file(repo_file)
    assert repo.exists


@pytest.mark.parametrize("packages", [("fping"), ("tmux2u")])
def test_packages(host, packages):
    """
    In the testing there are a couple of packages that are installed to
    verify that the repos are installed and working. This ensures that
    those packages are installed.
    """
    package = host.package(packages)

    assert package.is_installed
