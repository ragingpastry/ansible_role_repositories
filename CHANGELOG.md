# 1.0.0 (2020-02-06)


### Features

* Add ability to set repo priorities ([325171f](https://gitlab.com/dreamer-labs/maniac/ansible_role_repositories/commit/325171f))
